function wsc {  # this insertion marker must be on the first line in wsc.ps1
                # if it's the first line in your $PROFILE,
                # feel free to add something above this function.
    param(
        [switch] [Parameter()] [Alias("e")] $Edit,
        [switch] [Parameter()] [Alias("b")] $Build,

        [string] [Parameter()] [Alias("w")] $WorkDir = '.',

        [switch] [Parameter()] [Alias("d")] $Detach,
        [switch] [Parameter()] [Alias("r")] $Reconnect,
        [switch] [Parameter()] [Alias("t")] $Terminate
    )
    $wsc_path = Join-Path $env:USERPROFILE 'wsc'
    $wsc_image_tag = 'wsc'
    $workdir_inside = '/w'

    if ($Edit) {
        & code $wsc_path
        return
    }
    if ($Build) {
        Push-Location $wsc_path
        Copy-Item -Recurse ../.ssh/ ./ssh/
        & podman build --tag $wsc_image_tag .
        Remove-Item -r ./ssh/
        Pop-Location
        return
    }

    $workdir_wsl = & wsl wslpath -a $WorkDir
    $project_name = $workdir_wsl.TrimEnd('/').Split('/')[-1]
    $container_name = "wsc_$project_name"
    
    if ($Reconnect) {
        & podman exec --interactive --tty $container_name bash
        return
    }
    if ($Terminate) {
        & podman container stop --time 0 $container_name
        return
    }

    $run_params = @(
        '--interactive', '--tty', '--rm',
        '--name', $container_name,
        '--volume', "${workdir_wsl}:$workdir_inside",
        '--workdir', $workdir_inside
    )
    if ($Detach) { $run_params += '--detach' }
    & podman run $run_params $wsc_image_tag
    if ($Detach) {
        Write-Output "${container_name}: 'wsc -r' to enter, 'wsc -t' to terminate"
    }
} # if you read this in your $PROFILE, do not put anything below this line!
