$payload = Get-Content (Join-Path $PSScriptRoot "wsc.ps1")
$ins_marker = $payload[0]
$ins_point = Select-String -SimpleMatch $ins_marker $PROFILE
$existing_content = if ($ins_point) {
    $wsc_begin_line = $ins_point.LineNumber - 1
    Get-Content $PROFILE | Select-Object -First $wsc_begin_line | Out-String
} else {
    "$(Get-Content -Raw $PROFILE)`n`n"
}

Set-Content $PROFILE "$existing_content$(($payload | Out-String).TrimEnd())"
